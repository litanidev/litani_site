(function(){
    $(document).ready(function(){

        $(".top__trigger").sideNav({
            menuWidth: 300
        });
        //$('.product__tabs').tabs();
        $('#feedback').submit(function(e){
            e.preventDefault();
            var _ = $(this),
                msg = _.serialize();
            $.ajax({
                type: 'POST',
                url: 'form.php',
                data: msg,
                success: function (data) {
                    alert('Ваше обращение отправлено.')
                },
                error: function (xhr, str) {
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            });
        });
        $('.owl-carousel').owlCarousel({
            items: 1,
            autoplay: true
        });

        $.post('http://litani.net/api/getCountUsers', '', function(data){
            $('.countMembers').html(data.data);
        }, 'json');

        $('.tp-about__link').click(function(e){
            e.preventDefault();
            if(!$(this).hasClass('_active')){
                $('.tp-about__link').removeClass('_active');
                $(this).addClass('_active');
            }
        });


        $('#organization').click(function(e){
            e.preventDefault();
            $('#org').arcticmodal();
        });



        //var countdown = $('.tp-countdown');
        //var count = {
        //    days: {
        //        count: countdown.find('.tp-countdown__box[data-type="d"] span'),
        //        label: countdown.find('.tp-countdown__box[data-type="d"] p'),
        //        text: ['дней', 'день', 'дня']
        //    },
        //    hours: {
        //        count: countdown.find('.tp-countdown__box[data-type="h"] span'),
        //        label: countdown.find('.tp-countdown__box[data-type="h"] p'),
        //        text: ['часов', 'час', 'часа']
        //    },
        //    minutes: {
        //        count: countdown.find('.tp-countdown__box[data-type="m"] span'),
        //        label: countdown.find('.tp-countdown__box[data-type="m"] p'),
        //        text: ['минут', 'минута', 'минуты']
        //    },
        //    seconds: {
        //        count: countdown.find('.tp-countdown__box[data-type="s"] span'),
        //        label: countdown.find('.tp-countdown__box[data-type="s"] p'),
        //        text: ['секунд', 'секунда', 'секунды']
        //    }
        //};
        //
        //
        //var getLabel = function(time, pos){
        //    time >= 10 ? count[pos].count.text(time) : count[pos].count.text('0' + time);
        //
        //    if ((time <= 20 && time >= 10) || (time % 10 == 0) || (time % 10 >= 5 && time % 10 <= 9)){
        //        count[pos].label.text(count[pos].text[0]);
        //    } else {
        //        if (time % 10 == 1){
        //            count[pos].label.text(count[pos].text[1]);
        //        } else if (time % 10 >= 2 && time % 10 <= 4){
        //            count[pos].label.text(count[pos].text[2]);
        //        }
        //    }
        //};
        //
        //$.getJSON('time.php', function(e){
        //    var date_start = new Date(e.date_now),
        //        date_end = new Date(e.date_end),
        //        diffTime = date_end - date_start,
        //        duration = moment.duration(diffTime, 'milliseconds'),
        //        interval = 1000;
        //
        //
        //    function timer(){
        //        duration = moment.duration(duration - interval, 'milliseconds');
        //        getLabel(duration.days(), 'days');
        //        getLabel(duration.hours(), 'hours');
        //        getLabel(duration.minutes(), 'minutes');
        //        getLabel(duration.seconds(), 'seconds');
        //
        //        if (duration.days() === 0 && duration.hours() === 0 && duration.minutes() === 0 && duration.seconds() === 0){
        //            clearInterval(countInterval);
        //            countdown.append('<div class="tp-countdown__title">СТАРТ</div>');
        //            $('.tp-countdown__box').addClass('_hide');
        //            setTimeout(function(){
        //                if (!(location.href.slice(-4) == '?v=1')){
        //                    location.href += '?v=1'
        //                }
        //            }, 30000)
        //        }
        //    }
        //    timer();
        //    var countInterval = setInterval(timer, interval);
        //});
    });
})();
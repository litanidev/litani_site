$(document).ready(function(){
    var revapi = jQuery("#page").revolution({
        sliderType:"standard",
        sliderLayout:"fullscreen",
        dottedOverlay:"none",
        delay:9000,
        disableProgressBar: 'on',
        navigation: {
            keyboardNavigation:"on",
            keyboard_direction: "vertical",
            mouseScrollNavigation:"on",
            mouseScrollReverse:"default",
            onHoverStop:"off",
            touch:{
                touchenabled:"on",
                swipe_threshold: 75,
                swipe_min_touches: 1,
                swipe_direction: "vertical",
                drag_block_vertical: false
            }
            //,
            //bullets: {
            //    enable:true,
            //    hide_onmobile:true,
            //    hide_under:778,
            //    style:"hermes",
            //    hide_onleave:false,
            //    direction:"vertical",
            //    h_align:"right",
            //    v_align:"center",
            //    h_offset:20,
            //    v_offset:0,
            //    space:5,
            //    tmp:''
            //}
        },
        responsiveLevels: [1921,1601,1281,992],
        visibilityLevels:[1921,1601,1281,992],
        gridwidth:[1920,1600,1280,992],
        gridheight:[1000,900,720,900],
        lazyType: "none",
        shadow: 0,
        spinner: "off",
        stopLoop:"on",
        stopAfterLoops:0,
        stopAtSlide: 1,
        shuffle: "off",
        autoHeight:"on"
        //debugMode: 'true'
    });

    if(device.desktop()){
        $('.menu__anchor._slide, .tp-main__btn').click(function(e){
            e.preventDefault();
            var _sliderOffset = jQuery("#page").data('offset');
            var slide = $(this).data('slide');
            revapi.revshowslide(parseInt(slide) + parseInt(_sliderOffset));
        });
        $('.top__anchor').click(function(e){
            e.preventDefault();
            var _sliderOffset = jQuery("#page").data('offset');
            revapi.revshowslide(1 + parseInt(_sliderOffset));
        });

        revapi.bind("revolution.slide.onchange",function (e,data) {
            var _index = data.currentslide.attr('id');
            $('body').removeAttr('class');
            if (_index > 0){
                $('body').addClass('activeSlide-' + _index);
            }
            var slides = $('.menu__anchor');
            slides.removeClass('_active');
            slides.filter('[data-slide=' + _index + ']').addClass('_active');
        });


        var resFlag;
        function winRes(){
            var win_w = $(window).width();
            if(win_w < 992){
                resFlag = false;
                console.log('Мелкий');
                revapi.revkill();
            } else {
                resFlag = true;
                console.log('Крупный');
                revapi.revredraw();
            }
        }
        function resSlide(){
            var win_w = $(window).width();
            if( (win_w < 992) && resFlag){
                resFlag = false;
                console.log('Мелкий');
                revapi.revkill();
            }
            if( (win_w > 992) && !resFlag){
                resFlag = true;
                console.log('Крупный');
                revapi.revredraw();
            }
        }
        winRes();

        $(window).resize(function(){
            resSlide()
        })
    }
    if(device.tablet() || device.mobile()){
        revapi.revkill();
    }
});
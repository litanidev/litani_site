module.exports = function(grunt){

    grunt.initConfig({
        compass: {
            dist: {
                options: {
                    config: 'config.rb'
                }
            }
        },
        jade: {
            compile: {
                options: {
                    data: {
                        debug: false
                    }
                },
                files: {
                    "app/index.html" : ["dist/jade/index.jade"],
                    "app/registration.html" : ["dist/jade/registration.jade"],
                    "app/business-start.html" : ["dist/jade/business-start.jade"],
                    "app/business-recreation.html" : ["dist/jade/business-recreation.jade"],
                    "app/business-auto.html" : ["dist/jade/business-auto.jade"],
                    "app/business-marketing.html" : ["dist/jade/business-marketing.jade"],
                    "app/business-study.html" : ["dist/jade/business-study.jade"]
                }
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: [
                    'bower_components/jquery/dist/jquery.js',
                    //'dist/js/jquery.min.js',
                    'bower_components/device.js/lib/device.js',
                    //'bower_components/fullpage.js/jquery.fullPage.js',
                    'dist/js/materialize.js',
                    'dist/js/jquery.arcticmodal-0.3.min.js',
                    //'dist/js/jquery.countdown.js',
                    'dist/js/moment.js',
                    'bower_components/owl.carousel/dist/owl.carousel.js',
                    'dist/js/revolution/jquery.themepunch.tools.min.js',
                    'dist/js/revolution/jquery.themepunch.revolution.js',
                    'dist/js/revolution/extentions/revolution.extension.actions.js',
                    'dist/js/revolution/extentions/revolution.extension.carousel.js',
                    'dist/js/revolution/extentions/revolution.extension.kenburn.js',
                    'dist/js/revolution/extentions/revolution.extension.layeranimation.js',
                    'dist/js/revolution/extentions/revolution.extension.migration.js',
                    'dist/js/revolution/extentions/revolution.extension.navigation.js',
                    'dist/js/revolution/extentions/revolution.extension.parallax.js',
                    'dist/js/revolution/extentions/revolution.extension.slideanims.js',
                    'dist/js/revolution/extentions/revolution.extension.video.js',
                    'dist/js/rev-init.js',
                    'dist/js/script.js'
                ],
                dest: 'app/js/build.js'
            }
        },
        uglify: {
            my_target: {
                files: {
                    'app/js/build.min.js': ['app/js/build.js']
                }
            }
        },
        imagemin: {
            dynamic: {                         // Another target
                files: [{
                    expand: true,                  // Enable dynamic expansion
                    cwd: 'dist/',                   // Src matches are relative to this path
                    src: [
                        'images/**/*.{png,jpg,gif}'],   // Actual patterns to match
                    dest: 'app/'                  // Destination path prefix
                }]
            }
        },

        modernizr: {
            dist: {
                "dest" : "app/js/modernizr-custom.js",
                "parseFiles": true,
                "customTests": [],
                //"devFile": "/PATH/TO/modernizr-dev.js",
                //"outputFile": "/PATH/TO/modernizr-output.js",
                "tests": [
                    // Tests
                ],
                "options": [
                    "setClasses"
                ],
                "uglify": true
            }
        },
        svgmin: {
            options: {
                plugins: [
                    {
                        removeViewBox: false
                    }, {
                        removeUselessStrokeAndFill: false
                    }, {
                        removeAttrs: {
                            attrs: ['xmlns']
                        }
                    }
                ]
            },
            dist: {
                files: {
                    'app/svg/pattern-top.svg': 'dist/svg/pattern-top.svg',
                    'app/svg/logo.svg': 'dist/svg/logo.svg',
                    'app/svg/logo-mini.svg': 'dist/svg/logo-mini.svg'
                }
            }
        },

        watch: {
            stylesheets: {
                files: ['dist/sass/**/*.scss'],
                tasks: ['compass']
            },
            templates: {
                files: ['dist/jade/**/*.{jade,html}'],
                tasks: ['jade']
            },
            scripts: {
                files: ['dist/js/**/*.js'],
                tasks: ['concat']
            },
            //imagemin: {
            //    files: ['dist/images/**/*.{png,jpg,gif}'],
            //    tasks: ['imagemin']
            //},
            //svgmin: {
            //    files: ['dist/svg/**/*.svg'],
            //    tasks: ['svgmin']
            //},
            minifyJS: {
                files: ['app/js/build.js'],
                tasks: ['uglify']
            }
        },
        browserSync: {
            bsFiles: {
                src : [
                    'app/css/*.css',
                    'app/*.html',
                    'app/js/*.js',
                    'app/images/**/*.{png,jpg,gif}'
                ]
            },
            options: {
                watchTask: true,
                server: './app'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-svgmin');
    grunt.loadNpmTasks("grunt-modernizr");



    grunt.registerTask('default', ['modernizr', 'compass', 'jade', 'concat', 'uglify', 'imagemin', 'svgmin',  'watch']);

};